%kostur mag. rada
\documentclass[12pt,a4paper,twoside]{report}
\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{cmap}
\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{graphicx}

\pagestyle{fancy}
\fancyhf{}
\rhead{\today}
\lhead{University of Rijeka}
\rfoot{Page \thepage}
\lfoot{\textit{In silico} development of novel DNA methyltransferase inhibitor \\ Patrik Nikolić}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}



\begin{document}
	\title{\textit{In silico} development of novel DNA methyltransferase inhibitor}
	\author{Patrik Nikolić \\ Department of Biotechnology \\ University of Rijeka}
	\date{\today}
	\maketitle
	
	\begin{abstract}
		DNA methylation is a fundamental mechanism in functional organization of the human genome. Accordingly, DNA methyltransferase Dnmt1, has been unsuccessfully targeted in different drug-design efforts for the last 30 years. Most of those failures can be attributed to inadequate understanding of dynamic structural changes that regulate Dnmt1 activity and its interaction with other molecules. Using a combination of different QM/MM/MD methods we have identified several novel mechanism-based inhibitors by exploiting dynamic structural changes that control interactions between Dnmt1 and its substrates. Typical simulation used 500 000 steps in total duration of 1 nsec on GPU-accelerated version of GROMACS software package on NVIDIA CUDA-enabled GPUs. The simulations identified substrate analogues that have preserved key interactions with the enzyme and showed that solvent molecules are crucial for interactions between Dnmt1 with its cofactor. Substrate is positioned in the active site by a dynamic active site loop that acts as substrate lock-and-press mechanism just as earlier indicated in the experimental studies. QM studies indentified several substrate analogues that have the highest reactivity towards active site cysteine. 
	\end{abstract}
	
	\section{Introduction}
	\subsection{Human Genome Organization}
	
	 The human genome sequence provides the underlying code for human biology. Despite intensive study, especially in identifying protein-coding genes, our understanding of the genome is far from complete, particularly with regard to non-coding RNAs, alternatively spliced transcripts and regulatory sequences. \cite{3} Human genome is comprised of very complex nuclear genome and very simple mithochondrial genome of only 37 genes. The DNA sequences of the eukaryotic genome  can be classified into several types, including single-copy protein-encoding genes, DNA that is present in more than one copy (repetitive sequences) and intergenic (spacer) DNA. The most complex of these are the repetitive sequences, some of which are functional and some of which are without function. \cite{1} %Functional repetitive sequences are classified into dispersed and/or tandemly repeated gene families that either encode proteins (and may include noncoding pseudogenes), or are important noncoding transcribed sequences such as the ribosomal RNA genes.
	 
	 Repetitive sequences with no known function include the various highly repeated satellite families, and the dispersed, moderately repeated transposable element families. The remainder of the genome consists of spacer DNA, which is simply a broad category of undefined DNA sequences. \cite{1}
	 
	 Functional genome element is defined as a discrete genome segment that encodes a defined product (for example, protein or non-coding RNA) or displays a reproducible biochemical signature (for example, protein binding or a specific chromatin structure).\cite{3}
	
	Sequence comparisons with other mammalian genomes and vertebrate genomes indicate that 5 percent of the human genome has been strongly conserved during evolution and is presumably functionally important. Protein-coding DNA sequences account for about 1-1,5 percent of the genome. The other $\sim4$ percent of strongly conserved genome sequences consists of non-protein-coding DNA sequences, including genes whose final products are functionally important RNA molecules. Although sequences that make non-protein-coding RNA have not generally been so well conserved during evolution, some of the regulatory sequences are much more strongly conserved than protein-coding sequences.\cite{2}
	
	%Protein-coding sequences frequently belong to families of related sequences that may be organized into clusters on one or more chromosomes or be dispersed throughout the genome. Such families have arisen by gene duplication during evoulution. The mechanisms giving rise to duplicated genes also give rise to non-functional gene-related sequences (\textit{pseudogenes}). \cite{2}
	
	Discovery that the human genome is transcribed to give tens of thousands of different \underline{noncoding RNA} transcripts, including whole new classes of tiny regulatory RNAs not previously identified in the draft human genome sequences published in 2001. has been unexpected. Even though we are close to unlocking complete inventory of human protein-coding genes, our knowledge of RNA genes remains undeveloped. It is clear, however, that RNA is functionally much more versatile than previously suspected. In addition to a rapidly increasing RNA genes, we have also become aware of huge numbers of pseudogene copies of RNA genes.\cite{2} 
	
	A very large fraction of the human nuclear genome, and other complex genomes, is made up of highly repetitive noncoding DNA sequences. A sizeable component is organized in tandem head-to-tail repeats, but the majority consists of interspersed repeats that have been copies from RNA transcripts in the cell by reverse transcriptase. There is a growing realization of the functional importance of such repeats. \cite{2}
	
	Mitochondria possess their own genome - a single type of small circular DNA-encoding some of the components needed for mitochondrial protein synthesis on mitochondrial ribosomes. However, most mitochondrial proteins are encoded by nuclear genes and are synthesized on cytoplasmic ribosomes before being imported into the mitochondria. \cite{2}
	
	\subsubsection{Human Nuclear Genome}
	
	The human nuclear genome is about 3 Gbp (giga-base-pairs) in size. It is distributed between 24 different types of linear double-stranded DNA molecule, each of which has histones and nonhistone proteins bound to it, constituting a chromosome. There are 22 types of autosome and two sex chromosomes, X and Y. Human chromosomes can easily be differentiated by chromosome banding, and have been classified into groups largely according to size and, to some extent, centromere position. (\cite{2})
	
	General build of human nuclear genome is as followed:
    \begin{itemize}
    	\item \textbf{Genes and gene-related sequences (unique or moderately repetitive)}
    	\begin{itemize}
    		\item Coding DNA: exons
    		\item Noncoding DNA
    		\begin{itemize}
    			\item Introns, UTR, promoters, enhancers, etc.
    			\item Pseudogenes: processed, nonprocessed
    			\item minroRNA genes
    		\end{itemize}
    	\end{itemize}
    	\item \textbf{Extragenic DNA}
    	\begin{itemize}
    		\item Unique or low copy numbers
    		\item Moderate or highly repetitive
    		\begin{itemize}
    			\item Tandem (clustered) repeats: satellite, minisatellite, microsatellite
    			\item Interspersed: retrotransposons, DNA transposons
    		\end{itemize}
    	\end{itemize}
    \end{itemize}
    
	Not all of the human nuclear genome has been sequenced. The Human Genome Project focused primarily on sequencing \underline{euchromatin}, the gene-rich, transcriptionally active regions of the nuclear genome that account for 2.9 Gbp. The other $\sim(200)$ Mbp is made up of permanently condensed and transcriptionally inactive (constitutive) heterochromatin. The heterochromatin is composed of log arrays of highly repetitive DNA that are very difficult to sequence accurately. For a similar reason, the long arrays of tandemly repeated transcription units encoding 28S, 18S and 5.8S rRNA were also not sequenced.\cite{2}
	
	The proportion of some combinations of nucleotides can vary considerably. Like other vertebrate nuclear genomes, the human nuclear genome has a conspicuous shortage of the dinucleotide CpG. However, certain small regions of transcriptionally active DNA have the expected CpG density and, significantly, are unmethylated or hypomethylated. \cite{2}
	
	More than a decade after the Human Genome Project delivered the first reference genome sequence, there is still very considerable uncertainty about the total human gene number. When the early analyses of the genome were reported in 2001, the gene catalog generated by the International Human Genome Sequencing Consortium was very much oriented toward protein-coding genes. Original estimates suggested more than 30 000 human protein-coding genes, most of which were gene predictions without any supportive experimental evidence. This number was an overestimate because of errors that were made in defining genes. \cite{2}
	
	To validate gene predictions supportive evidence was sought, mostly by evolutinary comparisons. Comparison with other mammalian genomes, such as those of the mouse and the dog, failed to identify counterparts of many of the originally predicted human genes. By late 2009 the estimated number of human protein-coding genes appeared to be stabilizing somewhere around 20 000 to 21 000, but huge uncertainty remained about the number of human RNA genes. RNA genes are difficult to identify by using computer programs to analyze genome sequences: there are no open reading frames to screen for and many RNA genes are very small and often not well conserved during evolution. There is also the problem of how to define an RNA gene - comprehensive analyses have recently suggested that the great majority of the genome (and probably at least 85 percent of nucleotides) is transcribed. It is currently unknown how much of the transcriptional activity is background noise and how much is functionally significant. \cite{2}
	
	By the end of 2010, evidence for at least 6000 human RNA genes had been obtained, including thousands of genes encoding long noncoding RNAs that are thought to be important in gene regulation. \cite{2}
	
	The combination of about 20 000 protein-coding genes and at least 6000 RNA genes gives a total of at least 26 000 human genes. This remains a provisional total gene number - defining RNA genes is challenging and it will be some time before an accurate human gene number is obtained. \cite{2}
	
	\subsection{Epigenetics}
	
	Chromatin is the macromoleculer complex of DNA and histone proteins, which provides the scaffold for the packaging of our entire genome. The basic functional unit of chromatin is the nucleosome. It contains 147 base pairs of DNA, which is wrapped around a histone octamer. Chromatin can be subdivided into two major regions: (1) heterochromatin, which is highly condensed, late to replicate and primarily contains inactive genes; and (2) euchromatin, which is relatively open and contains most of the active genes. Efforts to study the coordinated regulation of the nucleosome have demonstrated that all of its components are subject to covalent modification, which fundamentally alters the organization and function of these basic tenants of chromatin. \cite{6}
	
	Epigenetics has been defined as the study of any potentially stable and, \underline{possibly},heritable change in gene expression or cellular phenotype that occurs without changes in the base-pairing of DNA. DNA methylation is the best studied of the epigenetic processes, yet still not understood. Hypomethylation of regulatory sequences tends to correlate with increased gene expression, while increased methylation usually results in transcriptional suppression. \cite{5}
	
	Epigenetic modifications in disease processes have been well studied. The largest body of literature is in the area of cancer research but such changes in the epigenome are increasingly being linked to other age-related diseases, such as Alzheimer disease and atherosclerosis. Epigenetic changes may also occur durng normal aging. The relationship between aging and DNA methylation was originally proposed in a study of humpbacked salmon; since then, the association of decreased global DNA methylation and age has been supported in studies of mouse, rat and human tissues. In a study of twins varying in age from 3-74 years, global and locus-specific epigenetic differences between the pairs were found to increase with age. However, the study did not follow the same individuals over time. Hypomethylation of repetitive elements (i.e.,Dna sequences mostly located in intergenic regions) has also been associated with age in epidemiologic studies. \cite{5}
	
	Modifications to DNA and histones are dynamically laid down and removed by chromatin-modifying enzymes in a highly regulated manner. There are now at least four different DNA modifications and 16 classes of histone modifications. These are described in \ref{table:1} and \ref{table:2}.
	\begin{table}
		\centering
		\small
		\begin{tabularx}{\textwidth}{||c | c | c||}
			\hline
			DNA modifications & Nomenclature & Attributed Function \\ \hline
			5-methycytosine & 5mC & transcription \\ 
			5-hydroxymethylcytosine & 5hmC & transcription \\ 
			5-formylcytosine & 5fC & unknown \\ \ 
			5-carboxylcytosine & 5caC & unknown \\ \hline
		\end{tabularx}
		\caption{Table of known DNA modifications}
		\label{table:1}
	\end{table}
	\begin{table}
		\centering
		\small
		\begin{tabularx}{\textwidth}{||l|l |l ||}
			\hline
			Histone Modification  & Nomenclature & Attributed Function  \\ \hline
			Acetylation  & K-ac  & transcription, repair, replication and condensation \\ 
			Methylation (lysine)    & K-me1, K-me2, K-me3   & transcription and repair   \\ 
			Methylation (arginine)  & R-me1, R-me2s, R-me2a & transcription  \\
			Phosphorylation  (serine and threonine) & S-ph, T-ph  & transcription, repair and condensation  \\
			Phosphorylation (tyrosine)  & Y-ph & transcription and repair  \\
			Ubiquitylation  & K-ub & transcription and repair   \\
			Sumoylation  & K-su  & transcription and repair   \\
			ADP ribosylation  & E-ar  & transcription and repair \\
			Deimination  & R -> cit  & transcription and decondesation \\
			Proline isomerisation & P-cis <-> P-trans & transcription  \\
			Crotonylation  & K-cr  & transcription \\
			Propionylation  & K-pr  & unknown  \\
			Butyrylation    & K-bu  & unknown  \\
			Formylation  & K-fo & unknown  \\
			Hydroxylation  & Y-oh   & unknown \\
			O-GlcNAcylation   & S-GlcNAc; T-GlcNAc    & trancstiption \\ \hline
		\end{tabularx}
		\caption {table of known histone modifications}
		\label{table:2}
	\end{table} These modifications can alter chromatin structure by altering noncovalent interactions within and between nucleosomes. They also serve as docking sites for specialized proteins with unique domains that specifically recognize these modifications. These chromatin readers recruit additional chromatin modifiers and remodeling enzymes, which serve as the effectors of the modification. \cite{6}
	
	The information conveyed by epigenetic modifications plays a critical role in the regulation of all DNA-based processes, such as transcription, DNA repair and replication. Consequently, abnormal expression patterns or genomic alterations in chromatin regulators can have profound results and can lead to the inductin and maintenance of various pathophysiological states. \cite{6}
	
	Our appreciation of epigenetic complexity and plasticity has dramatically increased over the last few years following the development of several global proteomic and genomic technologies. The coupling of next-generation sequencing (NGS) platforms with established chromatin techniques such as chromatin immunoprecipitation has presented us with a previously unparalleled view of the epigenome. These technologies have provided comprehensive maps of nucleosome positioning, chromatin conformation, transcription factor binding sites, the localization of histone and DNA modifications. It is understood nowadays that most of our genome is transcribed and that noncoding RNA may play a fundamental role in epigenetic regulation. \cite{6}
	
	\subsubsection{Epigenetics and cancer}
	
	As it has been already mentioned, epigenetic modifications affect two major pathophysiological processes - cancer and age-related diseases. The earliest indications of an epigenetic link to cancer were derived from gene expression and DNA methylation studies. Although many of these initial studies were purely correlative, they did highlight a potential connection between epigenetic pathways and cancer. \cite{6} 
	
	For instance, malignancies such as follicular lymphoma contain recurrent mutations of the histone methyltransferase \textit{MLL2} in close to 90 percent of cases. Similarly, \textit{UTX}, a histone demethylase, is mutated in up to 12 histologically distinct cancers. \cite{6}
	
	One way in which altered DNA methylation may play a role is by mediating the increased risk for certain pathologies that are typical for aged organism. The most striking alteration in the methylome (total area of methylated DNA) appears to be the emergence of regions of age-associated hyper- and hypomethylation. Interestingly, these age related shifts carry a considerable resemblence to the methylations profile of diseases typical of old age, such as cancer. Based on these observations, it is now believed that the age-related shifts in patterns of DNA methylation are a main contributor to the process of carcinogenesis. \cite{7}
	
	\subsubsection{Epigenetics and age-related diseases}
	
	There appears to be extensive shifts in the patterns of DNA methylation that occur over the lifetime of an organism. Since DNA methylation plays a central role in gene regulation and expression, such changes might influence the behaviour of the cell and potentially contribute to the aging process. \cite{7}
	
	Due to extensive research into the extent and consequences of alterations in the methylome, the link betwen aging and DNA methylation has been increasingly recognized and it is now widely established that changes to DNA methylation that occur with age contribute to age related diseases. However, although the occurrence and consequences of DNA methylation modifications have been and are being extensively researched, its causes remain shrouded in mystery. \cite{7}
	
	\subsection{DNA methylation}
	
	DNA methylation refers to the addition of a methyl group to the 5-carbon atom of a base in DNA, most commonly a cytosine residue linked to a guanine residue through a phosphate group. Due to the symmetry of these CpG sites, methylated cytosines come in pairs, with one on each strand. This provides a blueprint through which methylation can be conserved during DNA replication, where each parental CpG site induces methylation of the corresponding nascent strand. CpG sites are relatively rare in the genome, except for certain genomic locations where CpGs occur in high concentration, so called CpG islands (CGIs). In mammals, CGIs remain generally unmethylated, whereas the majority of all other CpG sites are methylated. Such CGIs mosty occur in promoter redions, although they can also be found in intragenic regions. In those cases where promoter CGIs are methylated, the corresponding gene is usually stably repressed. This has led to the general view that an increase in DNA methylation indicated higher levels of gene repression. This notion has been challenged by the recent observation that intragenic CpG methylation is positively correlated with active gene expression. \cite{7}
	
	Mapping of CpG islands has shown that they co-localize with the promoters of all constitutively expressed genes and approximately 40 percent of tissue-specific genes. There has even been suggestion that DNA replication origins coincide with CpG island in mammalian genome. Unmethylated CpG islands thus might serve to help the transcriptional and replication machinety to distinguish transcriptionally permissive, early replicating chromatin regions from the bulk of CpG methylated DNA. \cite{8}
	
	Large scale studies of DNA methylation patterns in mammalian somatic cells have revealed that a small fraction of CpG islands are methylated and confer the silencing of genes on the inactive X chromosome, imprinted genes, germ line-specific genes and other tissue- and development-specific genes. A small, bt noteworthy number of CpG islands are also differentially methylated amongst different tissue and cell types. One of the most striking examples of this differential methylation is observed between germ line and somatic tissues, whereby the CpG islands of germ line-specific genes are hypomethylated in cells of the germ line but methylated in somatic tissues. \cite{8}
	
	As becomes evident from the above, the function of DNA methylation depends largely on its context. Methylation of CpG sites at promoters and intragenic regions seem to be important in gene repression and activation, respectively, suggesting that methylation inhibits initiation of transcription but not elongation. This is in line with the supposed role of DNA methylation of silencing repetitive elements within genes, such as retroviruses and LINE1 elements. In this way, the methylation blocks transcription of these elements, while allowing elongation along the gene. Other proposed functions of DNA methylation include the regulation of splicing, modulation of the activity of enhancers and maintaining chromosomal stability. \cite{7}
	
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{SAM}
		\caption{The chemical structure of S-adenosyl methionine (SAM) - a methyl donor during the methylation of a cysteine residue}
		\label{fig:1}
	\end{figure}
	
	In order for these functions to be properly fulfilled, the DNA methylome needs to be accurately established during development and maintained after birth. The most important enzymes in these processes are the DNA methyltransferases (DNMTs), which catalyze the addition of the methyl group to cytosine. In humans, there are four different DNMTs: DNMT3a and DNMT3b are the \textit{de novo} methyltransferases that are responsible for establishing the DNA methylome during development, although it has recently been found that these enzymes also play a role in the methylation maintenance; DNMT1 is \underline{the main enzyme} responsible for the maintenance of the methylation pattern during DNA replication; and DNMT3L, lacking a catalytic domain, stimulates the activity of DNMT3a and DNMAT3b by binding these enzymes, thereby increasing their interaction with DNA and the methyl donor. This methyl donor is a small molecule called S-adenosyl methionine (SAM), which is made from adenosine triphosphate (ATP) and methionine. The methyl group on SAM is very reactive and can be transferred to the fifth carbon atom of cytosine by the catalytic action of the DNMTs.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{SAM_reakcija}
		\caption{During DNA methylation, a DNA methyltransferase catalyzes the transfer of a methyl residue from the sulfur atom of SAM to the 5-carbon atom of a cytosine base. In the figure, R1 represents the rest of the DNA molecule to which the cytosine is attached, and R2 and R3 represent the rest of SAM.}
		\label{fig:2}
	\end{figure}
	
	\subsubsection{Changes in the DNA methylome with age}
	
	Since all cells contain the same DNA, epigenetic control is of paramount importance in establishing the vast spectrum of different cell types present in an organism. It has been shown that DNA methylation may contribute to this diversity through differential methylation patterns across tissues. Soon after the first evidence for tissue-specific DNA methylomes and a role for DNA methylation in differentiation, it was shown that tumors also display a unique methylation pattern with high levels of hypomethylation. Since age is a major demographic risk factor for many cancers, research has since been conducted on the aging methylome and its link to cancer and other age-related diseases, such as Alzheimer's or Parkinson's disease. \cite{7}
	
	Over the past decade, many studies have been performed that compared the methylomes of populations varying in age, the largest of which comprised the analysis of more than 450 000 CpG markers of whole blood in a population of 656 people. Although the results depended on the genomic location and number of the CpG loci studied, these studies showed that up to third of the CpG sites are affected by age. \cite{7}
	
	Importantly, there appears to be a link between genotype and changes in DNA methylation. Christensen \textit{et al.} \cite{7} showed that 5 percent of the CpG loci affected by age was associated with a single nucleotide polymorphism (SNP). Such genetic variants that influence the methylation status of age-associated CpG-s are called \underline{methylation quantitative-trat loci (meQTLs)}. Some of these meQTLs were located in genes previously associated with longevity and aging. The researchers therefore concluded that in a small portion of genes, genetic and methylation effects may impart age-related phenotypes, either through independent mechanisms or through genotype-phenotype associations mediated by DNA methylation. \cite{7}
	
	\subsubsection{DNA methylation maintenance}
	
	During development, DNA methylation is carefully orchestrated and patterns of DNA methylation are reprogrammed genome-wide in germ celss and in pre-implantation embryos. After a genome-wide erasure, new patterns of methylation are re-established shortly after implantation with a wave of global \textit{de novo} methylation. \cite{8}
	
	In mammals, DNA methylation is maintained by Dnmt1. This methyltransferase is associated with replication foci and functions to restore hemimethylated DNA generated during DNA replication to the fully methylated state. Early studies illustrated that Dnmt1 is recruited to replication foci via an interaction with the proliferating cell nuclear antigen (PCNA) component of the replication machinery. However, disruption of this interaction only resulted in a minor reduction in DNA methylation. Recently, it was shown that Dnmt1 also interacts wih another chromatin associated protein, Ubiquitin-like PHD nad RING finger domain 1 (UHRF1), and that UHRF1 is required for the association of Dnmt1 with chromatin. Studies showing that mutation in UHRF1 cause severe decreases in DNA methylation, and that the SRA domain of UHRF1 specifically binds to hemimethylated CG dinucletides have lead to a model in which UHRF1 recruits Dnmt1 to hemimethlyated DNA. In addition, UHRF1 also interacts with Dnmt3a and Dnmt3b, which may suggest a role for UHRF1 in \textit{de novo} methylation. Maintenance of DNA methylation also requires the chromatin remodeling factor Lymphoid-specific Helicase (LSH1), although the mechanism through which LSH1 functions in DNA methylation remains unknown. \cite{10}
	
	Focus of this thesis will be to describe the development of novel inhibitor which should specifically and dose-dependentally inhibit Dnmt1 function. Novel inhibitor is intended to be used in research of various pathophysiological conditions caused by hypermethylation (described above) and treatment of those pathophysiological conditions.
	
	
	\subsection{Enzymology of Dnmt1}
	
	
\end{document}



